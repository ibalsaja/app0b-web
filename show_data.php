<?php

include 'koneksi.php';

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
    $nama = $_POST['nama'];
    $sql = "SELECT m.id, m.nama_sepatu, p.jenis_sepatu, concat('http://localhost/app0b-web/images/',photo)as url
    FROM sepatu m, tipe p
    WHERE m.jenis_sepatu=p.id_jenis";
    
    $result = mysqli_query($conn,$sql);
    $x = mysqli_num_rows($result);
    if($x>0){
        header("Access-Control-Allow-Origin: *");
        header("Content-type: application/json; charset=UTF-8");
        $data_sepatu = array();
        while($sepatu = mysqli_fetch_assoc($result)){
            array_push($data_sepatu, $sepatu);
        }
        echo json_encode($data_sepatu);
    }

}
?>